#include "tas.hpp"

#include <exception>
#include <iostream>
#include <sstream>

int Tas::parent(int n) {
  //a remplacer
  return 0 ;
}

int Tas::gauche(int n) {
  //a remplacer
  return 0 ;
}

int Tas::droite(int n) {
  //a remplacer
  return 0 ;
}

int Tas::niveau(int n) {
  //a remplacer
  return 0 ;
}

int Tas::min() {
  //a remplacer
  return 0 ;
}

int Tas::max() {
  //a remplacer
  return 0 ;
}

bool Tas::parent_valide(bool niveau_max, int valeur, int valeur_parent) {
  //a remplacer
  return false ;
}

bool Tas::grand_parent_valide(bool niveau_max, int valeur, int valeur_grand_parent) {
  //a remplacer
  return false ;
}

void Tas::structure_valide() {
  //taille du tas
  int size = tas.size() ;

  //valider tous les éléments du tas
  for(int i = 0; i < size; ++i) {
    //type de niveau de l'élément
    bool max_lvl = niveau(i) % 2 ;

    //validité du parent
    if( i > 0 && !parent_valide(max_lvl, tas[i], tas[parent(i)])) {
      std::stringstream ss ;
      ss << "le parent " << tas[parent(i)] << " de " << tas[i] << " n'est pas correct" ;
      throw std::domain_error(ss.str()) ;
    }

    //validité du grand parent
    if( i > 2 && !grand_parent_valide(max_lvl, tas[i], tas[parent(parent(i))])) {
      std::stringstream ss ;
      ss << "le grand parent " << tas[parent(parent(i))] << " de " << tas[i] << " n'est pas correct" ;
      throw std::domain_error(ss.str()) ;
    }
  }
}

void Tas::remonter(int n) {
  //a remplacer
  return ;
}

void Tas::inserer(int val) {
  //a remplacer
  return ;
}

int Tas::meilleur_enfant(bool niveau_max, int n) {
  //a remplacer
  return 0 ;
}

void Tas::enterrer(int n) {
  //a remplacer
  return ;
}

int Tas::pop_min() {
  //a remplacer
  return 0 ;
}

int Tas::pop_max() {
  //a remplacer
  return 0 ;
}

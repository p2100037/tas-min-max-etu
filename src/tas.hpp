#include <vector>

struct Tas {

  //parent de l'élément à l'indice n
  int parent(int n) ;

  //enfant gauche de l'élément à l'indice n
  int gauche(int n) ;

  //enfant droit de l'élément à l'indice n
  int droite(int n) ;

  //niveau de l'élément à l'indice n
  int niveau(int n) ;

  //minimum du tas
  int min() ;

  //maximum du tas
  int max() ;

  //validité du parent :
  //  * niveau_max : type de niveau de la valeur
  //  * valeur : valeur de l'élément
  //  * valeur_parent : valeur du parent à valider
  bool parent_valide(bool niveau_max, int valeur, int valeur_parent) ;

  //validité du grand parent :
  //  * niveau_max : type de niveau de la valeur
  //  * valeur : valeur de l'élément
  //  * valeur_grand_parent : valeur du grand parent à valider
  bool grand_parent_valide(bool niveau_max, int valeur, int valeur_grand_parent) ;

  //validité de tout le tas
  void structure_valide() ;

  //remonter un élément à l'indice n mal positionné dans le tas
  void remonter(int n) ;

  //insertion d'une valeur dans le tas
  void inserer(int val) ;

  //meilleur enfant pour prendre la place de l'élément n
  //  * niveau max : type de niveau à l'indice n
  //  * retourne l'indice de l'enfant
  int meilleur_enfant(bool niveau_max, int n) ;

  //descendre un élément à l'indice n mal positionné dans le tas
  void enterrer(int n) ;

  //extraction du minimum
  int pop_min() ;

  //extraction du maximum
  int pop_max() ;

  //stockage du tas
  std::vector<int> tas ;
} ;
